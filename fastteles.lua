script_name("Fast Teles")
script_version("2.4")
local imgui = require 'imgui'
local vkeys = require 'vkeys'
local encoding = require "encoding"
local inicfg = require 'inicfg'

local rkeys = require 'rkeys'

imgui.HotKey = require('imgui_addons').HotKey
-- inicfg

local directIni = "telesconfig.ini"

local ini = inicfg.load(inicfg.load({
    settings = {
        theme = 2,
		key = "[18,50]",
		fastmovest = true
    },
}, directIni))
inicfg.save(ini, directIni)

local tag = "{E800FF}[Fast Teles v2.33]{ffffff} "

encoding.default = 'CP1251'
u8 = encoding.UTF8

require 'lib.moonloader'
local sampev = require 'lib.samp.events'

require 'lib.moonloader'

local x, y = getScreenResolution()
local ActiveMenu = {
    v = decodeJson(ini.settings.key)
}
local tLastKeys = {}
local bindID = 0
local fmoves = imgui.ImBool(false)
local fmovescb = imgui.ImBool(ini.settings.fastmovest)
local main_window_state = imgui.ImBool(false)


function main()
	if not isSampLoaded() or not isSampfuncsLoaded() then return end
	while not isSampAvailable() do wait(100) end

    sampAddChatMessage(tag .."Autor: {E800FF}Xaero{FFFFFF}. Para ver los teles disponibles utiliza: {E800FF}/fteles{FFFFFF} o presiona: {E800FF}" .. table.concat(rkeys.getKeysName(ActiveMenu.v), " + "), -1)

local ip, port = sampGetCurrentServerAddress()
	if  ip ~= '54.39.139.249' then
			sampAddChatMessage(tag .."Servidor 'Galaxy-RP' no detectado, apagando. En caso de ser un error informar a @xaeroxq en discord.", -1)
            script:unload()
	end

	sampRegisterChatCommand("fteles", function ()
		main_window_state.v = not main_window_state.v
	end)
	bindID = rkeys.registerHotKey(ActiveMenu.v, true, function ()
	if not sampIsChatInputActive() or sampIsDialogActive() or isSampfuncsConsoleActive() then
		main_window_state.v = not main_window_state.v
	end
end)
	sampRegisterChatCommand("irp", function (arg)
		sampSendChat("/irpuerta " ..arg)
	end)
	sampRegisterChatCommand("irn", function (arg)
		sampSendChat("/irnegocio " ..arg)
	end)
	sampRegisterChatCommand("ire", function (arg)
		sampSendChat("/irempresa " ..arg)
	end)
	sampRegisterChatCommand("irl", function (arg)
		sampSendChat("/irlocal " ..arg)
	end)
	sampRegisterChatCommand("irc", function (arg)
		sampSendChat("/ircasa " ..arg)
	end)

	while true do
		wait(0)
        imgui.Process = main_window_state.v
		local ini = inicfg.load(nil,directIni)
if ini.settings.fastmovest then
	if isKeyJustPressed(0x6D) and not (sampIsChatInputActive() or sampIsDialogActive() or isSampfuncsConsoleActive()) then
		local x, y, z = getCharCoordinates(playerPed)
		setCharCoordinates(playerPed, x, y, z - 3)
	end
	if isKeyJustPressed(0x6B) and not (sampIsChatInputActive() or sampIsDialogActive() or isSampfuncsConsoleActive()) then
		local x, y, z = getCharCoordinates(playerPed)
		setCharCoordinates(playerPed, x, y, z + 3)
	end
	if isKeyJustPressed(0x62) and not (sampIsChatInputActive() or sampIsDialogActive() or isSampfuncsConsoleActive()) then
		local x, y, z = getCharCoordinates(playerPed)
		setCharCoordinates(playerPed, x, y - 3, z - 1)
	end
	if isKeyJustPressed(0x64) and not (sampIsChatInputActive() or sampIsDialogActive() or isSampfuncsConsoleActive()) then
		local x, y, z = getCharCoordinates(playerPed)
		setCharCoordinates(playerPed, x - 3, y, z - 1)
	end
	if isKeyJustPressed(0x66) and not (sampIsChatInputActive() or sampIsDialogActive() or isSampfuncsConsoleActive()) then
		local x, y, z = getCharCoordinates(playerPed)
		setCharCoordinates(playerPed, x + 3, y, z - 1)
	end
	if isKeyJustPressed(0x68) and not (sampIsChatInputActive() or sampIsDialogActive() or isSampfuncsConsoleActive()) then
		local x, y, z = getCharCoordinates(playerPed)
		setCharCoordinates(playerPed, x, y + 3, z - 1)
	end
end
		
		if ini.settings.theme == 2 then
			style2()
		end
		if ini.settings.theme == 3 then
			style3()
		end
		if ini.settings.theme == 4 then
			style4()
		end
		if ini.settings.theme == 5 then
			style5()
		end
		if ini.settings.theme == 6 then
			style6()
		end
		if ini.settings.theme == 7 then
			style7()
		end
		if ini.settings.theme == 8 then
			style8()
		end
		if ini.settings.theme == 9 then
			style9()
		end
		if ini.settings.theme == 10 then
			style10()
		end
		if ini.settings.theme == 11 then
			style11()
		end
		if ini.settings.theme == 12 then
			style12()
		end
		if ini.settings.theme == 13 then
			style13()
		end
		if ini.settings.theme == 14 then
			style14()
		end
    end
end


function save()
    inicfg.save(ini, directIni)
end

local font = nil
function imgui.BeforeDrawFrame()
	if font == nil then
    font = imgui.GetIO().Fonts:AddFontFromFileTTF(getFolderPath(20) .. "\\trebucbd.ttf", 20, nil, imgui.GetIO().Fonts:GetGlyphRangesCyrillic())
	end
end

function imgui.OnDrawFrame()

	if main_window_state.v then
	cursor = main_window_state
    if isKeyDown(17) and cursor == true then
        imgui.ShowCursor = false
    elseif not isKeyDown(17) and cursor == false and main_window_state then
        imgui.ShowCursor = true
    end
    imgui.SetNextWindowPos(imgui.ImVec2(x / 2, y / 2), imgui.Cond.FirstUseEver, imgui.ImVec2(0.5, 0.5))
    imgui.SetNextWindowSize(imgui.ImVec2(390, 390), imgui.Cond.FirstUseEver)
	

	local clrs = {
                                imgui.ImVec4(1.00, 0.28, 0.28, 1.00),
                                imgui.ImVec4(0.98, 0.43, 0.26, 1.00),
                                imgui.ImVec4(0.26, 0.98, 0.85, 1.00),
                                imgui.ImVec4(0.10, 0.09, 0.12, 1.00),
                                imgui.ImVec4(0.41, 0.19, 0.63, 1.00),
                                imgui.ImVec4(0.10, 0.09, 0.12, 1.00),
                                imgui.ImVec4(0.20, 0.25, 0.29, 1.00),
                                imgui.ImVec4(0.457, 0.200, 0.303, 1.00),
                                imgui.ImVec4(0.00, 0.69, 0.33, 1.00),
                                imgui.ImVec4(0.46, 0.11, 0.29, 1.00),
                                imgui.ImVec4(0.13, 0.75, 0.55, 1.00),
                                imgui.ImVec4(0.73, 0.36, 0.00, 1.00),
                                imgui.ImVec4(0.26, 0.59, 0.98, 1.00),
                            }
		
    imgui.Begin("Fast Teles   v2.32", main_window_state, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoResize)	
	if imgui.CollapsingHeader(u8"Temas") then
		imgui.SetCursorPosX(7)
		imgui.PushStyleColor(imgui.Col.CheckMark, clrs[1]);
		if imgui.RadioButton(u8'##tema1', ini.settings.theme ~= 2 and false or true) then
            ini.settings.theme = 2
			if inicfg.save(ini, directIni) then
			end
			theme(2)
		end
		imgui.PopStyleColor();
		imgui.SameLine()
		imgui.PushStyleColor(imgui.Col.CheckMark, clrs[2]);
		if imgui.RadioButton(u8'##tema2', ini.settings.theme ~= 3 and false or true) then
            ini.settings.theme = 3
			if inicfg.save(ini, directIni) then
			end
			theme(3)
		end
		imgui.PopStyleColor();
		imgui.SameLine()
		imgui.PushStyleColor(imgui.Col.CheckMark, clrs[3]);
		if imgui.RadioButton(u8'##tema4', ini.settings.theme ~= 4 and false or true) then
            ini.settings.theme = 4
			if inicfg.save(ini, directIni) then
			end
			theme(4)
		end
		imgui.PopStyleColor();
		imgui.SameLine()
		imgui.PushStyleColor(imgui.Col.CheckMark, clrs[4]);
		if imgui.RadioButton(u8'##tema5', ini.settings.theme ~= 5 and false or true) then
            ini.settings.theme = 5
			if inicfg.save(ini, directIni) then
			end
			theme(5)
		end
		imgui.PopStyleColor();		
		imgui.SameLine()
		imgui.PushStyleColor(imgui.Col.CheckMark, clrs[5]);
		if imgui.RadioButton(u8'##tema6', ini.settings.theme ~= 6 and false or true) then
            ini.settings.theme = 6
			if inicfg.save(ini, directIni) then
			end
			theme(6)
		end
		imgui.PopStyleColor();		
		imgui.SameLine()
		imgui.PushStyleColor(imgui.Col.CheckMark, clrs[6]);
		if imgui.RadioButton(u8'##tema7', ini.settings.theme ~= 7 and false or true) then
            ini.settings.theme = 7
			if inicfg.save(ini, directIni) then
			end
			theme(7)
		end
		imgui.PopStyleColor();
		imgui.SameLine()
		imgui.PushStyleColor(imgui.Col.CheckMark, clrs[7]);
		if imgui.RadioButton(u8'##tema8', ini.settings.theme ~= 8 and false or true) then
            ini.settings.theme = 8
			if inicfg.save(ini, directIni) then
			end
			theme(8)
		end
		imgui.PopStyleColor();
		imgui.SameLine()
		imgui.PushStyleColor(imgui.Col.CheckMark, clrs[8]);
		if imgui.RadioButton(u8'##tema9', ini.settings.theme ~= 9 and false or true) then
            ini.settings.theme = 9
			if inicfg.save(ini, directIni) then
			end
			theme(9)
		end
		imgui.PopStyleColor();
		imgui.SameLine()
		imgui.PushStyleColor(imgui.Col.CheckMark, clrs[9]);
		if imgui.RadioButton(u8'##tema10', ini.settings.theme ~= 10 and false or true) then
            ini.settings.theme = 10
			if inicfg.save(ini, directIni) then
			end
			theme(10)
		end
		imgui.PopStyleColor();
		imgui.SameLine()
		imgui.PushStyleColor(imgui.Col.CheckMark, clrs[10]);
		if imgui.RadioButton(u8'##tema11', ini.settings.theme ~= 11 and false or true) then
            ini.settings.theme = 11
			if inicfg.save(ini, directIni) then
			end
			theme(11)
		end
		imgui.PopStyleColor();
		imgui.SameLine()
		imgui.PushStyleColor(imgui.Col.CheckMark, clrs[11]);
		if imgui.RadioButton(u8'##tema12', ini.settings.theme ~= 12 and false or true) then
            ini.settings.theme = 12
			if inicfg.save(ini, directIni) then
			end
			theme(12)
		end
		imgui.PopStyleColor();
		imgui.SameLine()
		imgui.PushStyleColor(imgui.Col.CheckMark, clrs[12]);
		if imgui.RadioButton(u8'##tema13', ini.settings.theme ~= 13 and false or true) then
            ini.settings.theme = 13
			if inicfg.save(ini, directIni) then
			end
			theme(13)
		end
		imgui.PopStyleColor();
		imgui.SameLine()
		imgui.PushStyleColor(imgui.Col.CheckMark, clrs[13]);
		if imgui.RadioButton(u8'##tema14', ini.settings.theme ~= 14 and false or true) then
            ini.settings.theme = 14
			if inicfg.save(ini, directIni) then
			end
			theme(14)
		end
		imgui.PopStyleColor();
	end

    imgui.Text(u8'Tecla de acceso rapido: ')
	imgui.SameLine()
	imgui.SetCursorPosX(145)
	imgui.Tooltip(u8'Preciona el boton a la derecha para cambiar la tecla de acceso rapido.')
    if imgui.HotKey("##active", ActiveMenu, tLastKeys, 100,20) then
		rkeys.changeHotKey(bindID, ActiveMenu.v)
		sampAddChatMessage(tag .. "Tecla para abrir el menu cambiada. Anterior: {E800FF}" .. table.concat(rkeys.getKeysName(tLastKeys.v), " + ") .. "{FFFFFF} | Nueva: {E800FF}" .. table.concat(rkeys.getKeysName(ActiveMenu.v), " + "), -1)
		ini.settings.key = encodeJson(ActiveMenu.v)
		if inicfg.save(ini, directIni) then
		end
    end
	imgui.SameLine()
	imgui.SetCursorPosX(300)
	imgui.Text(u8'Changelog')
	imgui.Tooltip(u8'2.4: Eliminada actualizacion automatica, ultima versión.\n2.33: Agregado copia automatica de IP [Mods 3+]\n2.32: Agregado tele a HQ de Hells Angels, categoria "HQ Facciones"\n       - Actualizado tele de la HQ de MS13.\n2.31: Agregados multiples interiores a la categoria "Interiores"\n2.30: Agregado tele a HQ de Weasel News (Categoria HQ Facciones)\n       - Agregados "FastMoves". \n2.22: Agregados "/irp", "/irn", "/ire", "/irc" e "/irl", acortadores \n         de "/irpuerta", "/irnegocio", etc. ')

    imgui.SetCursorPosX(120)
    if imgui.Button(u8'Norte', imgui.ImVec2(40, 23)) then
		local x, y, z = getCharCoordinates(playerPed)
		setCharCoordinates(playerPed, x, y + 3, z - 1)
	end
	
	imgui.SameLine()
    imgui.SetCursorPosX(220)
    if imgui.Button(u8'Arriba', imgui.ImVec2(50, 23)) then
		local x, y, z = getCharCoordinates(playerPed)
		setCharCoordinates(playerPed, x, y, z + 3)
	end

    imgui.SetCursorPosX(90)
    if imgui.Button(u8'Oeste', imgui.ImVec2(40, 23)) then
		local x, y, z = getCharCoordinates(playerPed)
		setCharCoordinates(playerPed, x - 3, y, z - 1)
	end

    imgui.SameLine()
    imgui.SetCursorPosX(150)
    if imgui.Button(u8'Este', imgui.ImVec2(40, 23)) then
		local x, y, z = getCharCoordinates(playerPed)
		setCharCoordinates(playerPed, x + 3, y, z - 1)
	end

    imgui.SetCursorPosX(120)
    if imgui.Button(u8'Sur', imgui.ImVec2(40, 23)) then
		local x, y, z = getCharCoordinates(playerPed)
		setCharCoordinates(playerPed, x, y - 3, z - 1)
	end

	imgui.SameLine()
    imgui.SetCursorPosX(220)
    if imgui.Button(u8'Abajo', imgui.ImVec2(50, 23)) then
		local x, y, z = getCharCoordinates(playerPed)
		setCharCoordinates(playerPed, x, y, z - 3)
	end
	imgui.SameLine()
    imgui.SetCursorPosX(280)
	if imgui.Checkbox('FastMoves', fmovescb) then
		ini.settings.fastmovest = fmovescb.v
		save()
	end
	imgui.Tooltip(u8'Teclas del NUMPAD:\n \n      8   -\n   4    6+\n      2   ')
        imgui.CenterText(u8'Mas Comunes')
        imgui.SetCursorPosX(30)
		if imgui.Button(u8'Hospital LS', imgui.ImVec2(100, 30)) then
			sampSendChat('/tele 1')
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'Hospital SF', imgui.ImVec2(100, 30)) then
			sampSendChat('/tele 2')
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
        if imgui.Button(u8'Hospital LV', imgui.ImVec2(100, 30)) then
            sampSendChat('/tele 3')
		end
		imgui.SetCursorPosX(30)
		if imgui.Button(u8'LSPD', imgui.ImVec2(100, 30)) then
			sampSendChat('/tele 4')
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'SFPD', imgui.ImVec2(100, 30)) then
			sampSendChat('/tele 5')
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
        if imgui.Button(u8'LVPD', imgui.ImVec2(100, 30)) then
            sampSendChat('/tele 6')
		end
		imgui.SetCursorPosX(30)
        if imgui.Button(u8'SASD', imgui.ImVec2(100, 30)) then
            sampSendChat('/tele 125')
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'Meca', imgui.ImVec2(100, 30)) then
			sampSendChat('/tele 8')
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
        if imgui.Button(u8' FBI ', imgui.ImVec2(100, 30)) then
            sampSendChat('/tele 15')
		end
		imgui.SetCursorPosX(30)
		if imgui.Button(u8' SAFA ', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 206.45265197754, 1908.7398681641, 18)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'Mercado Negro', imgui.ImVec2(100, 30)) then
			sampSendChat('/tele 31')
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'C. Coutt', imgui.ImVec2(100, 30)) then
        	setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 2131.537109375, -1141.8687744141, 25.071756362915)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SetCursorPosX(30)
		if imgui.Button(u8'C. Maritimo', imgui.ImVec2(100, 30)) then
        	setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 841.94500732422, -2062.5915527344, 13)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
        if imgui.Button(u8'C. Grotti', imgui.ImVec2(100, 30)) then
        	setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 542.15466308594, -1290.7648925781, 17.6)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'C. Otto Autos', imgui.ImVec2(100, 30)) then
			sampSendChat('/tele 45')
		end
		imgui.SetCursorPosX(30)
		if imgui.Button(u8'C. SanFierro', imgui.ImVec2(100, 30)) then
			sampSendChat('/tele 44')
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
        if imgui.Button(u8'C. Aereo', imgui.ImVec2(100, 30)) then
            sampSendChat('/tele 16')
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'C. AutoBahn', imgui.ImVec2(100, 30)) then
        	setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 2201.171875, 1389.6416015625, 10.9)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SetCursorPosX(30)
		if imgui.Button(u8'Taller OD', imgui.ImVec2(100, 30)) then
			sampSendChat('/tele 10')
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
        if imgui.Button(u8'Plaza LS', imgui.ImVec2(100, 30)) then
        	setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 1479.0439453125, -1624.6611328125, 14)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'A. de Disparo', imgui.ImVec2(100, 30)) then
        	setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 1739.3756103516, -1587.4989013672, 14)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
	if imgui.CollapsingHeader(u8"Trabajos") then
        imgui.SetCursorPosX(30)
        if imgui.Button(u8'Camionero', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
            setCharCoordinates(PLAYER_PED, 1635.0970458984, 2354.466796875, 11)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'Pescador', imgui.ImVec2(100, 30)) then
			sampSendChat('/tele 30')
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'Basurero', imgui.ImVec2(100, 30)) then
			sampSendChat('/tele 69')
		end
		imgui.SetCursorPosX(30)
        if imgui.Button(u8'Uber', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
            setCharCoordinates(PLAYER_PED, 1755.4755859375, -1903.0168457031, 14)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'Medico', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 1177.9467773438, -1323.4158935547, 14.2)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'Trailero', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, -49.764770507813, -272.26126098633, 7)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
				imgui.SetCursorPosX(30)
        if imgui.Button(u8'Granjero', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
            setCharCoordinates(PLAYER_PED, -379.75326538086, -1438.9537353516, 26)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'R. de Productos', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 1086.3897705078, -1226.8444824219, 16)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'R. de Pizzas', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 2100.90234375, -1823.5559082031, 14)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SetCursorPosX(30)
        if imgui.Button(u8'Petrolero', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
            setCharCoordinates(PLAYER_PED, 617.72729492188, 1227.1075439453, 12)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'F. Materiales', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 1703.7912597656, 949.8984375, 11)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'F. Armas', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 2581.5112304688, 1198.3782958984, 11)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
	end
	if imgui.CollapsingHeader(u8"HQ Facciones") then
        imgui.SetCursorPosX(30)
        if imgui.Button(u8' LSPD ', imgui.ImVec2(100, 30)) then
            sampSendChat('/tele 4')
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'LSPD Dil', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 631.45825195313, -571.66497802734, 16.4)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'LSPD DIC', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 1696.4906005859, -1667.9108886719, 20.2)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SetCursorPosX(30)
		if imgui.Button(u8' SFPD ', imgui.ImVec2(100, 30)) then
			sampSendChat('/tele 5')
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
        if imgui.Button(u8'SFPD AP', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
            setCharCoordinates(PLAYER_PED, -2163.43359375, -2387.6137695313, 31)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'SFPD METRO', imgui.ImVec2(100, 30)) then
			sampSendChat('/tele 126')
		end
		imgui.SetCursorPosX(30)
		if imgui.Button(u8'SFPD DIC', imgui.ImVec2(100, 30)) then
			sampSendChat('/tele 137')
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8' LVPD ', imgui.ImVec2(100, 30)) then
			sampSendChat('/tele 6')
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'LVPD BigEar', imgui.ImVec2(100, 30)) then
			sampSendChat('/tele 141')
		end
		imgui.SetCursorPosX(30)
        if imgui.Button(u8' SASD ', imgui.ImVec2(100, 30)) then
            sampSendChat('/tele 125')
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
        if imgui.Button(u8'FBI LS', imgui.ImVec2(100, 30)) then
            sampSendChat('/tele 15')
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'FBI LV', imgui.ImVec2(100, 30)) then
			sampSendChat('/tele 19')
		end
		imgui.SetCursorPosX(30)
		if imgui.Button(u8'Hell Angels', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 2770.5703125, -1624.1359863281, 11)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end 
		imgui.SameLine()
		imgui.SetCursorPosX(145)
        if imgui.Button(u8'Yakuza', imgui.ImVec2(100, 30)) then
            sampSendChat('/tele 127')
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'LCN', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 2636, -1428, 31)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SetCursorPosX(30)
		if imgui.Button(u8'C.D. Sinaloa', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, -688.11871337891, 950.44610595703, 12.45)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'MS-13', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 2794.1940917969, 2226.0629882813, 10.9)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
        if imgui.Button(u8'Weasel News', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 940.0, 1732.4, 8.8)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SetCursorPosX(30)
		if imgui.Button(u8'SAFA', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 206.45265197754, 1908.7398681641, 18)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'SAFA SF', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, -1529.3460693359, 476.05902099609, 7.25)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'IGTV', imgui.ImVec2(100, 30)) then
			sampSendChat('/tele 64')
		end
	end
	if imgui.CollapsingHeader(u8"Pueblos | Ciudades") then
		imgui.SetCursorPosX(136)
        imgui.CenterText(u8'Los Santos + Pueblos')
        imgui.SetCursorPosX(30)
        if imgui.Button(u8'Centro LS', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
            setCharCoordinates(PLAYER_PED, 1664.0474853516, -1492.9104003906, 14)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'Dillimore', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 689.54803466797, -537.52954101563, 16.9)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'Palomino', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 2337.7783203125, 45.410633087158, 27)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
        imgui.SetCursorPosX(88)
        if imgui.Button(u8'Montgomery', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
            setCharCoordinates(PLAYER_PED, 1307.3182373047, 308.94549560547, 20)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(202)
		if imgui.Button(u8'Blueberry', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 226.49850463867, -142.21173095703, 2)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
        imgui.Separator()
        imgui.SetCursorPosX(130)
        imgui.CenterText(u8'Las Venturas + Pueblos')
        imgui.SetCursorPosX(30)
        if imgui.Button(u8'Centro LV', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
            setCharCoordinates(PLAYER_PED, 1885.4616699219, 1704.8720703125, 11.2)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'Fort Carson', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, -130.77545166016, 1105.01953125, 20.1)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'Las Barrancas', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, -815.12738037109, 1548.1584472656, 27.6)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
        imgui.SetCursorPosX(88)
        if imgui.Button(u8'Las Payasadas', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
            setCharCoordinates(PLAYER_PED, -248.73715209961, 2692.8981933594, 63)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
        imgui.SameLine()
        imgui.SetCursorPosX(202)
		if imgui.Button(u8'El Quebrados', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, -1488.15234375, 2609.1706542969, 56.1)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
        imgui.Separator()
        imgui.SetCursorPosX(136)
        imgui.CenterText(u8'San Fierro + Pueblos')
        imgui.SetCursorPosX(30)
        if imgui.Button(u8'Centro SF', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
            setCharCoordinates(PLAYER_PED, -2236.1577148438, 553.99145507813, 35.5)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'Angel Pine', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, -2147.3820800781, -2399.90625, 31)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
        		imgui.SetCursorPosX(260)
		if imgui.Button(u8'Bayside', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, -2474.2915039063, 2338.2561035156, 5.3)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
    end
	if imgui.CollapsingHeader(u8"Conquistas") then
		imgui.SetCursorPosX(165)
        imgui.CenterText(u8'San Fierro')
        imgui.SetCursorPosX(30)
        if imgui.Button(u8'Whetstone AP', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
            setCharCoordinates(PLAYER_PED, -1857,-1614,22)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'Garcia', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, -2205,54,36)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'Barco SF', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, -2417,1548,32)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SetCursorPosX(30)
        if imgui.Button(u8'Beacon Hill', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
            setCharCoordinates(PLAYER_PED, -371,-1043,60)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'San Fierro', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, -1426,-963,201)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'Monte Ch.', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, -2811,-1562,141)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.Separator()
		imgui.SetCursorPosX(165)
        imgui.CenterText(u8'Los Santos')
		imgui.SetCursorPosX(30)
        if imgui.Button(u8'Panopticon', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
            setCharCoordinates(PLAYER_PED, -542,-88,63)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'Fern Ridge', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 867,-32,63)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'East LS', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 2411,-1362,25)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SetCursorPosX(30)
        if imgui.Button(u8'Ganton', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
            setCharCoordinates(PLAYER_PED, 2315,-1821,14)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'Ocean Docks', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 2455.3063964844, -2583.0395507813, 13.9)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
        if imgui.Button(u8'Playa Seville', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
            setCharCoordinates(PLAYER_PED, 2781,-2020,14)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.Separator()
		imgui.SetCursorPosX(158)
        imgui.CenterText(u8'Las Venturas')
		imgui.SetCursorPosX(30)
        if imgui.Button(u8'Sobell R.Y.', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
            setCharCoordinates(PLAYER_PED, 2855,1646,11)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'Barrancas', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, -777,1556,27)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'Rockshore E', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 2830,934,11)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SetCursorPosX(30)
        if imgui.Button(u8'Prickle Pine', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
            setCharCoordinates(PLAYER_PED, 1811,2798,11.2)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'K.A.C.C.', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 2576,2823,11)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
	end
	if imgui.CollapsingHeader(u8"Fabricas") then

		imgui.SetCursorPosX(30)
		if imgui.Button(u8'Easter Basin', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, -1699.4739990234, -95.135223388672, 4)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'Bayside', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, -2432.89453125, 2258.3347167969, 5.2)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'Come a Lot', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 2499.3186035156, 965.39093017578, 13)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SetCursorPosX(30)
		if imgui.Button(u8"Camel's Toe", imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 2474.6181640625, 1245.8718261719, 11.1)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'Spinybed', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 2372.0163574219, 2758.3708496094, 11.1)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'Blueberry', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 279.82037353516, -219.60810852051, 1.8)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
        imgui.SetCursorPosX(30)
        if imgui.Button(u8'Tierra R.', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
            setCharCoordinates(PLAYER_PED, -550.21447753906, 2593.6447753906, 54.2)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'P. Seville', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, 2783.4221191406, -1952.7767333984, 13.8)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'Whetstone', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
			setCharCoordinates(PLAYER_PED, -1446.9088134766, -1530.5732421875, 102)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SetCursorPosX(145)
        if imgui.Button(u8'Flint County', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
            setCharCoordinates(PLAYER_PED, -32.075782775879, -1122.7609863281, 1.1)

		end
	end
	if imgui.CollapsingHeader(u8"Interiores") then
        imgui.SetCursorPosX(30)
        if imgui.Button(u8'Andromada', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (9))
			setInteriorVisible(9)
            setCharCoordinates(PLAYER_PED, 315.45, 976.59, 1960.85)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'Pista', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (7))
			setInteriorVisible(7)
            setCharCoordinates(PLAYER_PED, -1403.01,-250.45,1043.53)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'Derby', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (15))
			setInteriorVisible(15)
            setCharCoordinates(PLAYER_PED, -1417.89,932.44,1041.53)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SetCursorPosX(30)
        if imgui.Button(u8'Moto Cross', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (4))
			setInteriorVisible(4)
            setCharCoordinates(PLAYER_PED, -1421.56,-663.82,1059.55)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'Casino', imgui.ImVec2(100, 30)) then
			sampSendChat('/tele 24')
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'RC BattleField', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (10))
			setInteriorVisible(10)
            setCharCoordinates(PLAYER_PED, -1129.89,1057.54,1346.41)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SetCursorPosX(30)
        if imgui.Button(u8'Kickstart', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (14))
			setInteriorVisible(14)
            setCharCoordinates(PLAYER_PED, -1420.42,1616.92,1052.53)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
		if imgui.Button(u8'Vice Stadium', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (16))
			setInteriorVisible(16)
            setCharCoordinates(PLAYER_PED, -1401.06,1265.37,1039.86)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'Oval Stadium', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (1))
			setInteriorVisible(1)
            setCharCoordinates(PLAYER_PED, -1402.66,106.38,1032.27)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SetCursorPosX(30)
        if imgui.Button(u8'Restaurant LC', imgui.ImVec2(100, 30)) then
            sampSendChat('/tele 7')
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
        if imgui.Button(u8'Base Maritima', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
            setCharCoordinates(PLAYER_PED, 13527.94140625, 18675.728515625, 14.7)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 107")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'Estacionamiento', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (18))
			setInteriorVisible(18)
            setCharCoordinates(PLAYER_PED, 1290.41,1.95,1001.02)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SetCursorPosX(30)
        if imgui.Button(u8'Carniceria', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (1))
			setInteriorVisible(1)
            setCharCoordinates(PLAYER_PED, 963.05,2159.75,1011.03)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
        if imgui.Button(u8'Int. Shamal', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (1))
			setInteriorVisible(1)
            setCharCoordinates(PLAYER_PED, 1.54,23.31,1199.59)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'LV Werehouse', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (0))
			setInteriorVisible(0)
            setCharCoordinates(PLAYER_PED, 1059.8,2081.6,10.8)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SetCursorPosX(30)
        if imgui.Button(u8'Burdel', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (3))
			setInteriorVisible(3)
            setCharCoordinates(PLAYER_PED, 964.57965087891, -53.213264465332, 1001.3)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
        if imgui.Button(u8'Casa rara', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (6))
			setInteriorVisible(6)
            setCharCoordinates(PLAYER_PED, 263.34191894531, 979.95141601563, 1083.8)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'Palacio BS', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (2))
			setInteriorVisible(2)
            setCharCoordinates(PLAYER_PED, 2536.53,-1294.84,1044.3)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 100")
		end
		imgui.SetCursorPosX(30)
        if imgui.Button(u8'Represa', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (17))
			setInteriorVisible(17)
            setCharCoordinates(PLAYER_PED, -959.51599121094, 1952.8449707031, 9)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 0")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(145)
        if imgui.Button(u8'Mansion MD', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (5))
			setInteriorVisible(5)
            setCharCoordinates(PLAYER_PED, 1267.84,-776.95,1091.90)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 100")
		end
		imgui.SameLine()
		imgui.SetCursorPosX(260)
		if imgui.Button(u8'M. Jefferson', imgui.ImVec2(100, 30)) then
			setCharInterior(PLAYER_PED, (15))
			setInteriorVisible(15)
            setCharCoordinates(PLAYER_PED, 2217.28,-1150.53,1025.79)
			sampSendChat('/setvw '..select(2, sampGetPlayerIdByCharHandle(playerPed)).." 100")
		end
	end
end
    imgui.End()
end

function theme(arg)
	if arg == '2' then
		ini.settings.theme = '2'
		if inicfg.save(ini, directIni) then
		end
		style2()
	end
	if arg == '3' then
		ini.settings.theme = '3'
		if inicfg.save(ini, directIni) then
		end
		style3()
	end
	if arg == '4' then
		ini.settings.theme = '4'
		if inicfg.save(ini, directIni) then
		end
		style4()
	end
	if arg == '5' then
		ini.settings.theme = '5'
		if inicfg.save(ini, directIni) then
		end
		style5()
	end
	if arg == '6' then
		ini.settings.theme = '6'
		if inicfg.save(ini, directIni) then
		end
		style3()
	end
	if arg == '7' then
		ini.settings.theme = '7'
		if inicfg.save(ini, directIni) then
		end
		style3()
	end
	if arg == '8' then
		ini.settings.theme = '8'
		if inicfg.save(ini, directIni) then
		end
		style8()
	end
	if arg == '9' then
		ini.settings.theme = '9'
		if inicfg.save(ini, directIni) then
		end
		style9()
	end
	if arg == '10' then
		ini.settings.theme = '10'
		if inicfg.save(ini, directIni) then
		end
		style10()
	end
	if arg == '11' then
		ini.settings.theme = '11'
		if inicfg.save(ini, directIni) then
		end
		style11()
	end
	if arg == '12' then
		ini.settings.theme = '12'
		if inicfg.save(ini, directIni) then
		end
		style12()
	end
	if arg == '13' then
		ini.settings.theme = '13'
		if inicfg.save(ini, directIni) then
		end
		style13()
	end
	if arg == '14' then
		ini.settings.theme = '14'
		if inicfg.save(ini, directIni) then
		end
		style14()
	end
end

function imgui.CenterText(text)
    imgui.SetCursorPosX(imgui.GetWindowSize().x / 2 - imgui.CalcTextSize(text).x / 2)
    imgui.Text(text)
end


function imgui.Tooltip(text)
    if imgui.IsItemHovered() then
        imgui.BeginTooltip()
        imgui.Text(text)
        imgui.EndTooltip()
    end
end

function sampev.onServerMessage(color, text)
	local nm, ip = string.match(text, "%IP de (.-) es%: (.*)")
	if nm and ip then
		sampAddChatMessage('IP copiada al portapapeles.', 0xFFB0B0B0)
		setClipboardText(ip)
	end
end

function style2()
    imgui.SwitchContext()

    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    local ImVec2 = imgui.ImVec2
	
    --==[ STYLE ]==--
    imgui.GetStyle().WindowPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().FramePadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().ItemSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().ItemInnerSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().TouchExtraPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().IndentSpacing = 5
    imgui.GetStyle().ScrollbarSize = 10
    imgui.GetStyle().GrabMinSize = 17

    --==[ ROUNDING ]==--
    imgui.GetStyle().WindowRounding = 5
    imgui.GetStyle().FrameRounding = 4
    imgui.GetStyle().ScrollbarRounding = 4
    imgui.GetStyle().GrabRounding = 2
	
    --==[ ALIGN ]==--
    imgui.GetStyle().WindowTitleAlign = imgui.ImVec2(0.5, 0.5)
    imgui.GetStyle().ButtonTextAlign = imgui.ImVec2(0.5, 0.5)
    
        colors[clr.FrameBg]                = ImVec4(0.48, 0.16, 0.16, 0.54)
        colors[clr.FrameBgHovered]         = ImVec4(0.98, 0.26, 0.26, 0.40)
        colors[clr.FrameBgActive]          = ImVec4(0.98, 0.26, 0.26, 0.67)
        colors[clr.TitleBg]                = ImVec4(0.04, 0.04, 0.04, 1.00)
        colors[clr.TitleBgActive]          = ImVec4(0.48, 0.16, 0.16, 1.00)
        colors[clr.TitleBgCollapsed]       = ImVec4(0.00, 0.00, 0.00, 0.51)
        colors[clr.CheckMark]              = ImVec4(0.98, 0.26, 0.26, 1.00)
        colors[clr.SliderGrab]             = ImVec4(0.88, 0.26, 0.24, 1.00)
        colors[clr.SliderGrabActive]       = ImVec4(0.98, 0.26, 0.26, 1.00)
        colors[clr.Button]                 = ImVec4(0.98, 0.26, 0.26, 0.40)
        colors[clr.ButtonHovered]          = ImVec4(0.98, 0.26, 0.26, 1.00)
        colors[clr.ButtonActive]           = ImVec4(0.98, 0.06, 0.06, 1.00)
        colors[clr.Header]                 = ImVec4(0.98, 0.26, 0.26, 0.31)
        colors[clr.HeaderHovered]          = ImVec4(0.98, 0.26, 0.26, 0.80)
        colors[clr.HeaderActive]           = ImVec4(0.98, 0.26, 0.26, 1.00)
        colors[clr.Separator]              = colors[clr.Border]
        colors[clr.SeparatorHovered]       = ImVec4(0.75, 0.10, 0.10, 0.78)
        colors[clr.SeparatorActive]        = ImVec4(0.75, 0.10, 0.10, 1.00)
        colors[clr.ResizeGrip]             = ImVec4(0.98, 0.26, 0.26, 0.25)
        colors[clr.ResizeGripHovered]      = ImVec4(0.98, 0.26, 0.26, 0.67)
        colors[clr.ResizeGripActive]       = ImVec4(0.98, 0.26, 0.26, 0.95)
        colors[clr.TextSelectedBg]         = ImVec4(0.98, 0.26, 0.26, 0.35)
        colors[clr.Text]                   = ImVec4(1.00, 1.00, 1.00, 1.00)
        colors[clr.TextDisabled]           = ImVec4(0.50, 0.50, 0.50, 1.00)
        colors[clr.WindowBg]               = ImVec4(0.06, 0.06, 0.06, 0.94)
        colors[clr.PopupBg]                = ImVec4(0.08, 0.08, 0.08, 0.94)
        colors[clr.Border]                 = ImVec4(0.43, 0.43, 0.50, 0.50)
        colors[clr.BorderShadow]           = ImVec4(0.00, 0.00, 0.00, 0.00)
        colors[clr.MenuBarBg]              = ImVec4(0.14, 0.14, 0.14, 1.00)
        colors[clr.ScrollbarBg]            = ImVec4(0.02, 0.02, 0.02, 0.53)
        colors[clr.ScrollbarGrab]          = ImVec4(0.31, 0.31, 0.31, 1.00)
        colors[clr.ScrollbarGrabHovered]   = ImVec4(0.41, 0.41, 0.41, 1.00)
        colors[clr.ScrollbarGrabActive]    = ImVec4(0.51, 0.51, 0.51, 1.00)
        colors[clr.PlotLines]              = ImVec4(0.61, 0.61, 0.61, 1.00)
        colors[clr.PlotLinesHovered]       = ImVec4(1.00, 0.43, 0.35, 1.00)
        colors[clr.PlotHistogram]          = ImVec4(0.90, 0.70, 0.00, 1.00)
        colors[clr.PlotHistogramHovered]   = ImVec4(1.00, 0.60, 0.00, 1.00)
end
function style3()
    imgui.SwitchContext()

    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    local ImVec2 = imgui.ImVec2
	
    --==[ STYLE ]==--
    imgui.GetStyle().WindowPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().FramePadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().ItemSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().ItemInnerSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().TouchExtraPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().IndentSpacing = 5
    imgui.GetStyle().ScrollbarSize = 10
    imgui.GetStyle().GrabMinSize = 17

    --==[ ROUNDING ]==--
    imgui.GetStyle().WindowRounding = 5
    imgui.GetStyle().FrameRounding = 4
    imgui.GetStyle().ScrollbarRounding = 4
    imgui.GetStyle().GrabRounding = 2
	
    --==[ ALIGN ]==--
    imgui.GetStyle().WindowTitleAlign = imgui.ImVec2(0.5, 0.5)
    imgui.GetStyle().ButtonTextAlign = imgui.ImVec2(0.5, 0.5)
    
        colors[clr.WindowBg]               = ImVec4(0.0, 0.0, 0.0, 1.00)
        colors[clr.FrameBg]                = ImVec4(0.48, 0.23, 0.16, 0.54)
        colors[clr.FrameBgHovered]         = ImVec4(0.98, 0.43, 0.26, 0.40)
        colors[clr.FrameBgActive]          = ImVec4(0.98, 0.43, 0.26, 0.67)
        colors[clr.TitleBg]                = ImVec4(0.04, 0.04, 0.04, 1.00)
        colors[clr.TitleBgActive]          = ImVec4(0.48, 0.23, 0.16, 1.00)
        colors[clr.TitleBgCollapsed]       = ImVec4(0.00, 0.00, 0.00, 0.51)
        colors[clr.CheckMark]              = ImVec4(0.98, 0.43, 0.26, 1.00)
        colors[clr.SliderGrab]             = ImVec4(0.88, 0.39, 0.24, 1.00)
        colors[clr.SliderGrabActive]       = ImVec4(0.98, 0.43, 0.26, 1.00)
        colors[clr.Button]                 = ImVec4(0.98, 0.43, 0.26, 0.40)
        colors[clr.ButtonHovered]          = ImVec4(0.98, 0.43, 0.26, 1.00)
        colors[clr.ButtonActive]           = ImVec4(0.98, 0.28, 0.06, 1.00)
        colors[clr.Header]                 = ImVec4(0.98, 0.43, 0.26, 0.31)
        colors[clr.HeaderHovered]          = ImVec4(0.98, 0.43, 0.26, 0.80)
        colors[clr.HeaderActive]           = ImVec4(0.98, 0.43, 0.26, 1.00)
        colors[clr.Separator]              = colors[clr.Border]
        colors[clr.SeparatorHovered]       = ImVec4(0.75, 0.25, 0.10, 0.78)
        colors[clr.SeparatorActive]        = ImVec4(0.75, 0.25, 0.10, 1.00)
        colors[clr.ResizeGrip]             = ImVec4(0.98, 0.43, 0.26, 0.25)
        colors[clr.ResizeGripHovered]      = ImVec4(0.98, 0.43, 0.26, 0.67)
        colors[clr.ResizeGripActive]       = ImVec4(0.98, 0.43, 0.26, 0.95)
        colors[clr.PlotLines]              = ImVec4(0.61, 0.61, 0.61, 1.00)
        colors[clr.PlotLinesHovered]       = ImVec4(1.00, 0.50, 0.35, 1.00)
        colors[clr.TextSelectedBg]         = ImVec4(0.98, 0.43, 0.26, 0.35)
        colors[clr.Text]                   = ImVec4(1.00, 1.00, 1.00, 1.00)
        colors[clr.TextDisabled]           = ImVec4(0.50, 0.50, 0.50, 1.00)
        colors[clr.PopupBg]                = ImVec4(0.08, 0.08, 0.08, 0.94)
        colors[clr.Border]                 = ImVec4(1.0, 1.0, 1.0, 0.10)
        colors[clr.BorderShadow]           = ImVec4(0.00, 0.00, 0.00, 0.00)
        colors[clr.MenuBarBg]              = ImVec4(0.14, 0.14, 0.14, 1.00)
        colors[clr.ScrollbarBg]            = ImVec4(0.02, 0.02, 0.02, 0.53)
        colors[clr.ScrollbarGrab]          = ImVec4(0.31, 0.31, 0.31, 1.00)
        colors[clr.ScrollbarGrabHovered]   = ImVec4(0.41, 0.41, 0.41, 1.00)
        colors[clr.ScrollbarGrabActive]    = ImVec4(0.51, 0.51, 0.51, 1.00)
        colors[clr.PlotHistogram]          = ImVec4(0.90, 0.70, 0.00, 1.00)
        colors[clr.PlotHistogramHovered]   = ImVec4(1.00, 0.60, 0.00, 1.00)
end

function style4()
    imgui.SwitchContext()

    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    local ImVec2 = imgui.ImVec2
	
    --==[ STYLE ]==--
    imgui.GetStyle().WindowPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().FramePadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().ItemSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().ItemInnerSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().TouchExtraPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().IndentSpacing = 5
    imgui.GetStyle().ScrollbarSize = 10
    imgui.GetStyle().GrabMinSize = 17

    --==[ ROUNDING ]==--
    imgui.GetStyle().WindowRounding = 5
    imgui.GetStyle().FrameRounding = 4
    imgui.GetStyle().ScrollbarRounding = 4
    imgui.GetStyle().GrabRounding = 2
	
    --==[ ALIGN ]==--
    imgui.GetStyle().WindowTitleAlign = imgui.ImVec2(0.5, 0.5)
    imgui.GetStyle().ButtonTextAlign = imgui.ImVec2(0.5, 0.5)
    
        colors[clr.WindowBg]               = ImVec4(0.0, 0.0, 0.0, 1.00)
        colors[clr.FrameBg]                = ImVec4(0.16, 0.48, 0.42, 0.54)
        colors[clr.FrameBgHovered]         = ImVec4(0.26, 0.98, 0.85, 0.40)
        colors[clr.FrameBgActive]          = ImVec4(0.26, 0.98, 0.85, 0.67)
        colors[clr.TitleBg]                = ImVec4(0.04, 0.04, 0.04, 1.00)
        colors[clr.TitleBgActive]          = ImVec4(0.16, 0.48, 0.42, 1.00)
        colors[clr.TitleBgCollapsed]       = ImVec4(0.00, 0.00, 0.00, 0.51)
        colors[clr.CheckMark]              = ImVec4(0.26, 0.98, 0.85, 1.00)
        colors[clr.SliderGrab]             = ImVec4(0.24, 0.88, 0.77, 1.00)
        colors[clr.SliderGrabActive]       = ImVec4(0.26, 0.98, 0.85, 1.00)
        colors[clr.Button]                 = ImVec4(0.26, 0.98, 0.85, 0.40)
        colors[clr.ButtonHovered]          = ImVec4(0.26, 0.98, 0.85, 1.00)
        colors[clr.ButtonActive]           = ImVec4(0.06, 0.98, 0.82, 1.00)
        colors[clr.Header]                 = ImVec4(0.26, 0.98, 0.85, 0.31)
        colors[clr.HeaderHovered]          = ImVec4(0.26, 0.98, 0.85, 0.80)
        colors[clr.HeaderActive]           = ImVec4(0.26, 0.98, 0.85, 1.00)
        colors[clr.Separator]              = colors[clr.Border]
        colors[clr.SeparatorHovered]       = ImVec4(0.10, 0.75, 0.63, 0.78)
        colors[clr.SeparatorActive]        = ImVec4(0.10, 0.75, 0.63, 1.00)
        colors[clr.ResizeGrip]             = ImVec4(0.26, 0.98, 0.85, 0.25)
        colors[clr.ResizeGripHovered]      = ImVec4(0.26, 0.98, 0.85, 0.67)
        colors[clr.ResizeGripActive]       = ImVec4(0.26, 0.98, 0.85, 0.95)
        colors[clr.PlotLines]              = ImVec4(0.61, 0.61, 0.61, 1.00)
        colors[clr.PlotLinesHovered]       = ImVec4(1.00, 0.81, 0.35, 1.00)
        colors[clr.TextSelectedBg]         = ImVec4(0.26, 0.98, 0.85, 0.35)
        colors[clr.Text]                   = ImVec4(1.00, 1.00, 1.00, 1.00)
        colors[clr.TextDisabled]           = ImVec4(0.50, 0.50, 0.50, 1.00)
        colors[clr.PopupBg]                = ImVec4(0.08, 0.08, 0.08, 0.94)
        colors[clr.Border]                 = ImVec4(1.0, 1.0, 1.0, 0.10)
        colors[clr.BorderShadow]           = ImVec4(0.00, 0.00, 0.00, 0.00)
        colors[clr.MenuBarBg]              = ImVec4(0.14, 0.14, 0.14, 1.00)
        colors[clr.ScrollbarBg]            = ImVec4(0.02, 0.02, 0.02, 0.53)
        colors[clr.ScrollbarGrab]          = ImVec4(0.31, 0.31, 0.31, 1.00)
        colors[clr.ScrollbarGrabHovered]   = ImVec4(0.41, 0.41, 0.41, 1.00)
        colors[clr.ScrollbarGrabActive]    = ImVec4(0.51, 0.51, 0.51, 1.00)
        colors[clr.PlotHistogram]          = ImVec4(0.90, 0.70, 0.00, 1.00)
        colors[clr.PlotHistogramHovered]   = ImVec4(1.00, 0.60, 0.00, 1.00)
end

function style5()
    imgui.SwitchContext()

    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    local ImVec2 = imgui.ImVec2
	
    --==[ STYLE ]==--
    imgui.GetStyle().WindowPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().FramePadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().ItemSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().ItemInnerSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().TouchExtraPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().IndentSpacing = 5
    imgui.GetStyle().ScrollbarSize = 10
    imgui.GetStyle().GrabMinSize = 17

    --==[ ROUNDING ]==--
    imgui.GetStyle().WindowRounding = 5
    imgui.GetStyle().FrameRounding = 4
    imgui.GetStyle().ScrollbarRounding = 4
    imgui.GetStyle().GrabRounding = 2
	
    --==[ ALIGN ]==--
    imgui.GetStyle().WindowTitleAlign = imgui.ImVec2(0.5, 0.5)
    imgui.GetStyle().ButtonTextAlign = imgui.ImVec2(0.5, 0.5)
    
        colors[clr.WindowBg]               = ImVec4(0.0, 0.0, 0.0, 1.00)
        colors[clr.Text]                   = ImVec4(0.80, 0.80, 0.83, 1.00)
        colors[clr.TextDisabled]           = ImVec4(0.24, 0.23, 0.29, 1.00)
        colors[clr.PopupBg]                = ImVec4(0.07, 0.07, 0.09, 1.00)
        colors[clr.Border]                 = ImVec4(1.0, 1.0, 1.0, 0.10)
        colors[clr.BorderShadow]           = ImVec4(0.92, 0.91, 0.88, 0.00)
        colors[clr.FrameBg]                = ImVec4(0.10, 0.09, 0.12, 0.54)
        colors[clr.FrameBgHovered]         = ImVec4(0.24, 0.23, 0.29, 1.00)
        colors[clr.FrameBgActive]          = ImVec4(0.56, 0.56, 0.58, 1.00)
        colors[clr.TitleBg]                = ImVec4(0.10, 0.09, 0.12, 1.00)
        colors[clr.TitleBgCollapsed]       = ImVec4(1.00, 0.98, 0.95, 0.75)
        colors[clr.TitleBgActive]          = ImVec4(0.07, 0.07, 0.09, 1.00)
        colors[clr.MenuBarBg]              = ImVec4(0.10, 0.09, 0.12, 1.00)
        colors[clr.ScrollbarBg]            = ImVec4(0.10, 0.09, 0.12, 1.00)
        colors[clr.ScrollbarGrab]          = ImVec4(0.80, 0.80, 0.83, 0.31)
        colors[clr.ScrollbarGrabHovered]   = ImVec4(0.56, 0.56, 0.58, 1.00)
        colors[clr.ScrollbarGrabActive]    = ImVec4(0.06, 0.05, 0.07, 1.00)
        colors[clr.CheckMark]              = ImVec4(0.80, 0.80, 0.83, 0.31)
        colors[clr.SliderGrab]             = ImVec4(0.80, 0.80, 0.83, 0.31)
        colors[clr.SliderGrabActive]       = ImVec4(0.06, 0.05, 0.07, 1.00)
        colors[clr.Button]                 = ImVec4(0.10, 0.09, 0.12, 1.00)
        colors[clr.ButtonHovered]          = ImVec4(0.24, 0.23, 0.29, 1.00)
        colors[clr.ButtonActive]           = ImVec4(0.56, 0.56, 0.58, 1.00)
        colors[clr.Header]                 = ImVec4(0.10, 0.09, 0.12, 1.00)
        colors[clr.HeaderHovered]          = ImVec4(0.56, 0.56, 0.58, 1.00)
        colors[clr.HeaderActive]           = ImVec4(0.06, 0.05, 0.07, 1.00)
        colors[clr.ResizeGrip]             = ImVec4(0.00, 0.00, 0.00, 0.00)
        colors[clr.ResizeGripHovered]      = ImVec4(0.56, 0.56, 0.58, 1.00)
        colors[clr.ResizeGripActive]       = ImVec4(0.06, 0.05, 0.07, 1.00)
        colors[clr.PlotLines]              = ImVec4(0.40, 0.39, 0.38, 0.63)
        colors[clr.PlotLinesHovered]       = ImVec4(0.25, 1.00, 0.00, 1.00)
        colors[clr.PlotHistogram]          = ImVec4(0.40, 0.39, 0.38, 0.63)
        colors[clr.PlotHistogramHovered]   = ImVec4(0.25, 1.00, 0.00, 1.00)
        colors[clr.TextSelectedBg]         = ImVec4(0.25, 1.00, 0.00, 0.43)
end

function style6()
    imgui.SwitchContext()

    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    local ImVec2 = imgui.ImVec2
	
    --==[ STYLE ]==--
    imgui.GetStyle().WindowPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().FramePadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().ItemSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().ItemInnerSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().TouchExtraPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().IndentSpacing = 5
    imgui.GetStyle().ScrollbarSize = 10
    imgui.GetStyle().GrabMinSize = 17

    --==[ ROUNDING ]==--
    imgui.GetStyle().WindowRounding = 5
    imgui.GetStyle().FrameRounding = 4
    imgui.GetStyle().ScrollbarRounding = 4
    imgui.GetStyle().GrabRounding = 2
	
    --==[ ALIGN ]==--
    imgui.GetStyle().WindowTitleAlign = imgui.ImVec2(0.5, 0.5)
    imgui.GetStyle().ButtonTextAlign = imgui.ImVec2(0.5, 0.5)
    
        colors[clr.WindowBg]               = ImVec4(0.0, 0.0, 0.0, 1.00)
        colors[clr.Text]                 = ImVec4(1.00, 1.00, 1.00, 1.00)
        colors[clr.TextDisabled]         = ImVec4(0.60, 0.60, 0.60, 1.00)
        colors[clr.PopupBg]              = ImVec4(0.09, 0.09, 0.09, 1.00)
        colors[clr.Border]                 = ImVec4(1.0, 1.0, 1.0, 0.10)
        colors[clr.BorderShadow]         = ImVec4(9.90, 9.99, 9.99, 0.00)
        colors[clr.FrameBg]              = ImVec4(0.34, 0.30, 0.34, 0.54)
        colors[clr.FrameBgHovered]       = ImVec4(0.22, 0.21, 0.21, 0.40)
        colors[clr.FrameBgActive]        = ImVec4(0.20, 0.20, 0.20, 0.44)
        colors[clr.TitleBg]              = ImVec4(0.52, 0.27, 0.77, 1.00)
        colors[clr.TitleBgActive]        = ImVec4(0.55, 0.28, 0.75, 1.00)
        colors[clr.TitleBgCollapsed]     = ImVec4(9.99, 9.99, 9.90, 0.20)
        colors[clr.MenuBarBg]            = ImVec4(0.27, 0.27, 0.29, 0.80)
        colors[clr.ScrollbarBg]          = ImVec4(0.30, 0.20, 0.39, 1.00)
        colors[clr.ScrollbarGrab]        = ImVec4(0.41, 0.19, 0.63, 0.31)
        colors[clr.ScrollbarGrabHovered] = ImVec4(0.41, 0.19, 0.63, 0.78)
        colors[clr.ScrollbarGrabActive]  = ImVec4(0.41, 0.19, 0.63, 1.00)
        colors[clr.CheckMark]            = ImVec4(0.89, 0.89, 0.89, 0.50)
        colors[clr.SliderGrab]           = ImVec4(1.00, 1.00, 1.00, 0.30)
        colors[clr.SliderGrabActive]     = ImVec4(0.80, 0.50, 0.50, 1.00)
        colors[clr.Button]               = ImVec4(0.41, 0.19, 0.63, 0.44)
        colors[clr.ButtonHovered]        = ImVec4(0.41, 0.19, 0.63, 1.00)
        colors[clr.ButtonActive]         = ImVec4(0.64, 0.33, 0.94, 1.00)
        colors[clr.Header]               = ImVec4(0.56, 0.27, 0.73, 0.44)
        colors[clr.HeaderHovered]        = ImVec4(0.78, 0.44, 0.89, 0.80)
        colors[clr.HeaderActive]         = ImVec4(0.81, 0.52, 0.87, 0.80)
        colors[clr.Separator]            = ImVec4(0.42, 0.42, 0.42, 1.00)
        colors[clr.SeparatorHovered]     = ImVec4(0.57, 0.24, 0.73, 1.00)
        colors[clr.SeparatorActive]      = ImVec4(0.69, 0.69, 0.89, 1.00)
        colors[clr.ResizeGrip]           = ImVec4(1.00, 1.00, 1.00, 0.30)
        colors[clr.ResizeGripHovered]    = ImVec4(1.00, 1.00, 1.00, 0.60)
        colors[clr.ResizeGripActive]     = ImVec4(1.00, 1.00, 1.00, 0.89)
        colors[clr.PlotLines]            = ImVec4(1.00, 0.99, 0.99, 1.00)
        colors[clr.PlotLinesHovered]     = ImVec4(0.49, 0.00, 0.89, 1.00)
        colors[clr.PlotHistogram]        = ImVec4(9.99, 9.99, 9.90, 1.00)
        colors[clr.PlotHistogramHovered] = ImVec4(9.99, 9.99, 9.90, 1.00)
        colors[clr.TextSelectedBg]       = ImVec4(0.54, 0.00, 1.00, 0.34)
end

function style7()
    imgui.SwitchContext()

    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    local ImVec2 = imgui.ImVec2
	
    --==[ STYLE ]==--
    imgui.GetStyle().WindowPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().FramePadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().ItemSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().ItemInnerSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().TouchExtraPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().IndentSpacing = 5
    imgui.GetStyle().ScrollbarSize = 10
    imgui.GetStyle().GrabMinSize = 17

    --==[ ROUNDING ]==--
    imgui.GetStyle().WindowRounding = 5
    imgui.GetStyle().FrameRounding = 4
    imgui.GetStyle().ScrollbarRounding = 4
    imgui.GetStyle().GrabRounding = 2
	
    --==[ ALIGN ]==--
    imgui.GetStyle().WindowTitleAlign = imgui.ImVec2(0.5, 0.5)
    imgui.GetStyle().ButtonTextAlign = imgui.ImVec2(0.5, 0.5)
    
        colors[clr.WindowBg]               = ImVec4(0.0, 0.0, 0.0, 1.00)
        colors[clr.Text]                   = ImVec4(0.80, 0.80, 0.83, 1.00)
        colors[clr.TextDisabled]           = ImVec4(0.24, 0.23, 0.29, 1.00)
        colors[clr.PopupBg]                = ImVec4(0.07, 0.07, 0.09, 1.00)
        colors[clr.Border]                 = ImVec4(1.0, 1.0, 1.0, 0.10)
        colors[clr.BorderShadow]           = ImVec4(0.92, 0.91, 0.88, 0.00)
        colors[clr.FrameBg]                = ImVec4(0.10, 0.09, 0.12, 0.54)
        colors[clr.FrameBgHovered]         = ImVec4(0.24, 0.23, 0.29, 1.00)
        colors[clr.FrameBgActive]          = ImVec4(0.56, 0.56, 0.58, 1.00)
        colors[clr.TitleBg]                = ImVec4(0.76, 0.31, 0.00, 1.00)
        colors[clr.TitleBgCollapsed]       = ImVec4(1.00, 0.98, 0.95, 0.75)
        colors[clr.TitleBgActive]          = ImVec4(0.80, 0.33, 0.00, 1.00)
        colors[clr.MenuBarBg]              = ImVec4(0.10, 0.09, 0.12, 1.00)
        colors[clr.ScrollbarBg]            = ImVec4(0.10, 0.09, 0.12, 1.00)
        colors[clr.ScrollbarGrab]          = ImVec4(0.80, 0.80, 0.83, 0.31)
        colors[clr.ScrollbarGrabHovered]   = ImVec4(0.56, 0.56, 0.58, 1.00)
        colors[clr.ScrollbarGrabActive]    = ImVec4(0.06, 0.05, 0.07, 1.00)
        colors[clr.CheckMark]              = ImVec4(1.00, 0.42, 0.00, 0.53)
        colors[clr.SliderGrab]             = ImVec4(1.00, 0.42, 0.00, 0.53)
        colors[clr.SliderGrabActive]       = ImVec4(1.00, 0.42, 0.00, 1.00)
        colors[clr.Button]                 = ImVec4(0.10, 0.09, 0.12, 1.00)
        colors[clr.ButtonHovered]          = ImVec4(0.24, 0.23, 0.29, 1.00)
        colors[clr.ButtonActive]           = ImVec4(0.56, 0.56, 0.58, 1.00)
        colors[clr.Header]                 = ImVec4(0.10, 0.09, 0.12, 1.00)
        colors[clr.HeaderHovered]          = ImVec4(0.56, 0.56, 0.58, 1.00)
        colors[clr.HeaderActive]           = ImVec4(0.06, 0.05, 0.07, 1.00)
        colors[clr.ResizeGrip]             = ImVec4(0.00, 0.00, 0.00, 0.00)
        colors[clr.ResizeGripHovered]      = ImVec4(0.56, 0.56, 0.58, 1.00)
        colors[clr.ResizeGripActive]       = ImVec4(0.06, 0.05, 0.07, 1.00)
        colors[clr.PlotLines]              = ImVec4(0.40, 0.39, 0.38, 0.63)
        colors[clr.PlotLinesHovered]       = ImVec4(0.25, 1.00, 0.00, 1.00)
        colors[clr.PlotHistogram]          = ImVec4(0.40, 0.39, 0.38, 0.63)
        colors[clr.PlotHistogramHovered]   = ImVec4(0.25, 1.00, 0.00, 1.00)
        colors[clr.TextSelectedBg]         = ImVec4(0.25, 1.00, 0.00, 0.43)
end

function style8()
    imgui.SwitchContext()

    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    local ImVec2 = imgui.ImVec2
	
    --==[ STYLE ]==--
    imgui.GetStyle().WindowPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().FramePadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().ItemSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().ItemInnerSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().TouchExtraPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().IndentSpacing = 5
    imgui.GetStyle().ScrollbarSize = 10
    imgui.GetStyle().GrabMinSize = 17

    --==[ ROUNDING ]==--
    imgui.GetStyle().WindowRounding = 5
    imgui.GetStyle().FrameRounding = 4
    imgui.GetStyle().ScrollbarRounding = 4
    imgui.GetStyle().GrabRounding = 2
	
    --==[ ALIGN ]==--
    imgui.GetStyle().WindowTitleAlign = imgui.ImVec2(0.5, 0.5)
    imgui.GetStyle().ButtonTextAlign = imgui.ImVec2(0.5, 0.5)
    
        colors[clr.WindowBg]               = ImVec4(0.0, 0.0, 0.0, 1.00)
        colors[clr.Text]                   = ImVec4(0.95, 0.96, 0.98, 1.00)
        colors[clr.TextDisabled]           = ImVec4(0.36, 0.42, 0.47, 1.00)
        colors[clr.PopupBg]                = ImVec4(0.08, 0.08, 0.08, 0.94)
        colors[clr.Border]                 = ImVec4(1.0, 1.0, 1.0, 0.10)
        colors[clr.BorderShadow]           = ImVec4(0.00, 0.00, 0.00, 0.00)
        colors[clr.FrameBg]                = ImVec4(0.20, 0.25, 0.29, 0.54)
        colors[clr.FrameBgHovered]         = ImVec4(0.12, 0.20, 0.28, 1.00)
        colors[clr.FrameBgActive]          = ImVec4(0.09, 0.12, 0.14, 1.00)
        colors[clr.TitleBg]                = ImVec4(0.09, 0.12, 0.14, 1.00)
        colors[clr.TitleBgCollapsed]       = ImVec4(0.00, 0.00, 0.00, 0.51)
        colors[clr.TitleBgActive]          = ImVec4(0.08, 0.10, 0.12, 1.00)
        colors[clr.MenuBarBg]              = ImVec4(0.15, 0.18, 0.22, 1.00)
        colors[clr.ScrollbarBg]            = ImVec4(0.02, 0.02, 0.02, 0.39)
        colors[clr.ScrollbarGrab]          = ImVec4(0.20, 0.25, 0.29, 1.00)
        colors[clr.ScrollbarGrabHovered]   = ImVec4(0.18, 0.22, 0.25, 1.00)
        colors[clr.ScrollbarGrabActive]    = ImVec4(0.09, 0.21, 0.31, 1.00)
        colors[clr.CheckMark]              = ImVec4(0.28, 0.56, 1.00, 1.00)
        colors[clr.SliderGrab]             = ImVec4(0.28, 0.56, 1.00, 1.00)
        colors[clr.SliderGrabActive]       = ImVec4(0.37, 0.61, 1.00, 1.00)
        colors[clr.Button]                 = ImVec4(0.20, 0.25, 0.29, 1.00)
        colors[clr.ButtonHovered]          = ImVec4(0.28, 0.56, 1.00, 1.00)
        colors[clr.ButtonActive]           = ImVec4(0.06, 0.53, 0.98, 1.00)
        colors[clr.Header]                 = ImVec4(0.20, 0.25, 0.29, 0.55)
        colors[clr.HeaderHovered]          = ImVec4(0.26, 0.59, 0.98, 0.80)
        colors[clr.HeaderActive]           = ImVec4(0.26, 0.59, 0.98, 1.00)
        colors[clr.ResizeGrip]             = ImVec4(0.26, 0.59, 0.98, 0.25)
        colors[clr.ResizeGripHovered]      = ImVec4(0.26, 0.59, 0.98, 0.67)
        colors[clr.ResizeGripActive]       = ImVec4(0.06, 0.05, 0.07, 1.00)
        colors[clr.PlotLines]              = ImVec4(0.61, 0.61, 0.61, 1.00)
        colors[clr.PlotLinesHovered]       = ImVec4(1.00, 0.43, 0.35, 1.00)
        colors[clr.PlotHistogram]          = ImVec4(0.90, 0.70, 0.00, 1.00)
        colors[clr.PlotHistogramHovered]   = ImVec4(1.00, 0.60, 0.00, 1.00)
        colors[clr.TextSelectedBg]         = ImVec4(0.25, 1.00, 0.00, 0.43)
end

function style9()
    imgui.SwitchContext()

    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    local ImVec2 = imgui.ImVec2
	
    --==[ STYLE ]==--
    imgui.GetStyle().WindowPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().FramePadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().ItemSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().ItemInnerSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().TouchExtraPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().IndentSpacing = 5
    imgui.GetStyle().ScrollbarSize = 10
    imgui.GetStyle().GrabMinSize = 17

    --==[ ROUNDING ]==--
    imgui.GetStyle().WindowRounding = 5
    imgui.GetStyle().FrameRounding = 4
    imgui.GetStyle().ScrollbarRounding = 4
    imgui.GetStyle().GrabRounding = 2
	
    --==[ ALIGN ]==--
    imgui.GetStyle().WindowTitleAlign = imgui.ImVec2(0.5, 0.5)
    imgui.GetStyle().ButtonTextAlign = imgui.ImVec2(0.5, 0.5)
    
        colors[clr.WindowBg]               = ImVec4(0.0, 0.0, 0.0, 1.00)
        colors[clr.Text]                   = ImVec4(0.860, 0.930, 0.890, 0.78)
        colors[clr.TextDisabled]           = ImVec4(0.860, 0.930, 0.890, 0.28)
        colors[clr.PopupBg]                = ImVec4(0.200, 0.220, 0.270, 0.9)
        colors[clr.Border]                 = ImVec4(1.0, 1.0, 1.0, 0.10)
        colors[clr.BorderShadow]           = ImVec4(0.00, 0.00, 0.00, 0.00)
        colors[clr.FrameBg]                = ImVec4(0.200, 0.220, 0.270, 0.54)
        colors[clr.FrameBgHovered]         = ImVec4(0.455, 0.198, 0.301, 0.78)
        colors[clr.FrameBgActive]          = ImVec4(0.455, 0.198, 0.301, 1.00)
        colors[clr.TitleBg]                = ImVec4(0.232, 0.201, 0.271, 1.00)
        colors[clr.TitleBgActive]          = ImVec4(0.502, 0.075, 0.256, 1.00)
        colors[clr.TitleBgCollapsed]       = ImVec4(0.200, 0.220, 0.270, 0.75)
        colors[clr.MenuBarBg]              = ImVec4(0.200, 0.220, 0.270, 0.47)
        colors[clr.ScrollbarBg]            = ImVec4(0.200, 0.220, 0.270, 1.00)
        colors[clr.ScrollbarGrab]          = ImVec4(0.09, 0.15, 0.1, 1.00)
        colors[clr.ScrollbarGrabHovered]   = ImVec4(0.455, 0.198, 0.301, 0.78)
        colors[clr.ScrollbarGrabActive]    = ImVec4(0.455, 0.198, 0.301, 1.00)
        colors[clr.CheckMark]              = ImVec4(0.71, 0.22, 0.27, 1.00)
        colors[clr.SliderGrab]             = ImVec4(0.47, 0.77, 0.83, 0.14)
        colors[clr.SliderGrabActive]       = ImVec4(0.71, 0.22, 0.27, 1.00)
        colors[clr.Button]                 = ImVec4(0.457, 0.200, 0.303, 1.00)
        colors[clr.ButtonHovered]          = ImVec4(0.455, 0.198, 0.301, 1.00)
        colors[clr.ButtonActive]           = ImVec4(0.455, 0.198, 0.301, 1.00)
        colors[clr.Header]                 = ImVec4(0.455, 0.198, 0.301, 0.76)
        colors[clr.HeaderHovered]          = ImVec4(0.455, 0.198, 0.301, 0.86)
        colors[clr.HeaderActive]           = ImVec4(0.502, 0.075, 0.256, 1.00)
        colors[clr.ResizeGrip]             = ImVec4(0.47, 0.77, 0.83, 0.04)
        colors[clr.ResizeGripHovered]      = ImVec4(0.455, 0.198, 0.301, 0.78)
        colors[clr.ResizeGripActive]       = ImVec4(0.455, 0.198, 0.301, 1.00)
        colors[clr.PlotLines]              = ImVec4(0.860, 0.930, 0.890, 0.63)
        colors[clr.PlotLinesHovered]       = ImVec4(0.455, 0.198, 0.301, 1.00)
        colors[clr.PlotHistogram]          = ImVec4(0.860, 0.930, 0.890, 0.63)
        colors[clr.PlotHistogramHovered]   = ImVec4(0.455, 0.198, 0.301, 1.00)
        colors[clr.TextSelectedBg]         = ImVec4(0.455, 0.198, 0.301, 0.43)
end

function style10()
    imgui.SwitchContext()

    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    local ImVec2 = imgui.ImVec2
	
    --==[ STYLE ]==--
    imgui.GetStyle().WindowPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().FramePadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().ItemSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().ItemInnerSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().TouchExtraPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().IndentSpacing = 5
    imgui.GetStyle().ScrollbarSize = 10
    imgui.GetStyle().GrabMinSize = 17

    --==[ ROUNDING ]==--
    imgui.GetStyle().WindowRounding = 5
    imgui.GetStyle().FrameRounding = 4
    imgui.GetStyle().ScrollbarRounding = 4
    imgui.GetStyle().GrabRounding = 2
	
    --==[ ALIGN ]==--
    imgui.GetStyle().WindowTitleAlign = imgui.ImVec2(0.5, 0.5)
    imgui.GetStyle().ButtonTextAlign = imgui.ImVec2(0.5, 0.5)
    
        colors[clr.WindowBg]               = ImVec4(0.0, 0.0, 0.0, 1.00)
        colors[clr.Text]                   = ImVec4(0.90, 0.90, 0.90, 1.00)
        colors[clr.TextDisabled]           = ImVec4(0.60, 0.60, 0.60, 1.00)
        colors[clr.PopupBg]                = ImVec4(0.08, 0.08, 0.08, 1.00)
        colors[clr.Border]                 = ImVec4(1.0, 1.0, 1.0, 0.10)
        colors[clr.BorderShadow]           = ImVec4(0.00, 0.00, 0.00, 0.00)
        colors[clr.FrameBg]                = ImVec4(0.15, 0.15, 0.15, 0.54)
        colors[clr.FrameBgHovered]         = ImVec4(0.19, 0.19, 0.19, 0.71)
        colors[clr.FrameBgActive]          = ImVec4(0.34, 0.34, 0.34, 0.79)
        colors[clr.TitleBg]                = ImVec4(0.00, 0.69, 0.33, 1.00)
        colors[clr.TitleBgActive]          = ImVec4(0.00, 0.74, 0.36, 1.00)
        colors[clr.TitleBgCollapsed]       = ImVec4(0.00, 0.69, 0.33, 0.50)
        colors[clr.MenuBarBg]              = ImVec4(0.00, 0.80, 0.38, 1.00)
        colors[clr.ScrollbarBg]            = ImVec4(0.16, 0.16, 0.16, 1.00)
        colors[clr.ScrollbarGrab]          = ImVec4(0.00, 0.69, 0.33, 1.00)
        colors[clr.ScrollbarGrabHovered]   = ImVec4(0.00, 0.82, 0.39, 1.00)
        colors[clr.ScrollbarGrabActive]    = ImVec4(0.00, 1.00, 0.48, 1.00)
        colors[clr.CheckMark]              = ImVec4(0.00, 0.69, 0.33, 1.00)
        colors[clr.SliderGrab]             = ImVec4(0.00, 0.69, 0.33, 1.00)
        colors[clr.SliderGrabActive]       = ImVec4(0.00, 0.77, 0.37, 1.00)
        colors[clr.Button]                 = ImVec4(0.00, 0.69, 0.33, 1.00)
        colors[clr.ButtonHovered]          = ImVec4(0.00, 0.82, 0.39, 1.00)
        colors[clr.ButtonActive]           = ImVec4(0.00, 0.87, 0.42, 1.00)
        colors[clr.Header]                 = ImVec4(0.00, 0.69, 0.33, 1.00)
        colors[clr.HeaderHovered]          = ImVec4(0.00, 0.76, 0.37, 0.57)
        colors[clr.HeaderActive]           = ImVec4(0.00, 0.88, 0.42, 0.89)
        colors[clr.Separator]              = ImVec4(1.00, 1.00, 1.00, 0.40)
        colors[clr.SeparatorHovered]       = ImVec4(1.00, 1.00, 1.00, 0.60)
        colors[clr.SeparatorActive]        = ImVec4(1.00, 1.00, 1.00, 0.80)
        colors[clr.ResizeGrip]             = ImVec4(0.00, 0.69, 0.33, 1.00)
        colors[clr.ResizeGripHovered]      = ImVec4(0.00, 0.76, 0.37, 1.00)
        colors[clr.ResizeGripActive]       = ImVec4(0.00, 0.86, 0.41, 1.00)
        colors[clr.PlotLines]              = ImVec4(0.00, 0.69, 0.33, 1.00)
        colors[clr.PlotLinesHovered]       = ImVec4(0.00, 0.74, 0.36, 1.00)
        colors[clr.PlotHistogram]          = ImVec4(0.00, 0.69, 0.33, 1.00)
        colors[clr.PlotHistogramHovered]   = ImVec4(0.00, 0.80, 0.38, 1.00)
        colors[clr.TextSelectedBg]         = ImVec4(0.00, 0.69, 0.33, 0.72)
end

function style11()
    imgui.SwitchContext()

    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    local ImVec2 = imgui.ImVec2
	
    --==[ STYLE ]==--
    imgui.GetStyle().WindowPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().FramePadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().ItemSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().ItemInnerSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().TouchExtraPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().IndentSpacing = 5
    imgui.GetStyle().ScrollbarSize = 10
    imgui.GetStyle().GrabMinSize = 17

    --==[ ROUNDING ]==--
    imgui.GetStyle().WindowRounding = 5
    imgui.GetStyle().FrameRounding = 4
    imgui.GetStyle().ScrollbarRounding = 4
    imgui.GetStyle().GrabRounding = 2
	
    --==[ ALIGN ]==--
    imgui.GetStyle().WindowTitleAlign = imgui.ImVec2(0.5, 0.5)
    imgui.GetStyle().ButtonTextAlign = imgui.ImVec2(0.5, 0.5)
    
        colors[clr.WindowBg]               = ImVec4(0.0, 0.0, 0.0, 1.00)
        colors[clr.FrameBg]                = ImVec4(0.46, 0.11, 0.29, 0.54)
        colors[clr.FrameBgHovered]         = ImVec4(0.69, 0.16, 0.43, 1.00)
        colors[clr.FrameBgActive]          = ImVec4(0.58, 0.10, 0.35, 1.00)
        colors[clr.TitleBg]                = ImVec4(0.00, 0.00, 0.00, 1.00)
        colors[clr.TitleBgActive]          = ImVec4(0.61, 0.16, 0.39, 1.00)
        colors[clr.TitleBgCollapsed]       = ImVec4(0.00, 0.00, 0.00, 0.51)
        colors[clr.CheckMark]              = ImVec4(0.94, 0.30, 0.63, 1.00)
        colors[clr.SliderGrab]             = ImVec4(0.85, 0.11, 0.49, 1.00)
        colors[clr.SliderGrabActive]       = ImVec4(0.89, 0.24, 0.58, 1.00)
        colors[clr.Button]                 = ImVec4(0.46, 0.11, 0.29, 1.00)
        colors[clr.ButtonHovered]          = ImVec4(0.69, 0.17, 0.43, 1.00)
        colors[clr.ButtonActive]           = ImVec4(0.59, 0.10, 0.35, 1.00)
        colors[clr.Header]                 = ImVec4(0.46, 0.11, 0.29, 1.00)
        colors[clr.HeaderHovered]          = ImVec4(0.69, 0.16, 0.43, 1.00)
        colors[clr.HeaderActive]           = ImVec4(0.58, 0.10, 0.35, 1.00)
        colors[clr.Separator]              = ImVec4(0.69, 0.16, 0.43, 1.00)
        colors[clr.SeparatorHovered]       = ImVec4(0.58, 0.10, 0.35, 1.00)
        colors[clr.SeparatorActive]        = ImVec4(0.58, 0.10, 0.35, 1.00)
        colors[clr.ResizeGrip]             = ImVec4(0.46, 0.11, 0.29, 0.70)
        colors[clr.ResizeGripHovered]      = ImVec4(0.69, 0.16, 0.43, 0.67)
        colors[clr.ResizeGripActive]       = ImVec4(0.70, 0.13, 0.42, 1.00)
        colors[clr.TextSelectedBg]         = ImVec4(1.00, 0.78, 0.90, 0.35)
        colors[clr.Text]                   = ImVec4(1.00, 1.00, 1.00, 1.00)
        colors[clr.TextDisabled]           = ImVec4(0.60, 0.19, 0.40, 1.00)
        colors[clr.PopupBg]                = ImVec4(0.08, 0.08, 0.08, 0.94)
        colors[clr.Border]                 = ImVec4(1.0, 1.0, 1.0, 0.10)
        colors[clr.BorderShadow]           = ImVec4(0.49, 0.14, 0.31, 0.00)
        colors[clr.MenuBarBg]              = ImVec4(0.15, 0.15, 0.15, 1.00)
        colors[clr.ScrollbarBg]            = ImVec4(0.02, 0.02, 0.02, 0.53)
        colors[clr.ScrollbarGrab]          = ImVec4(0.31, 0.31, 0.31, 1.00)
        colors[clr.ScrollbarGrabHovered]   = ImVec4(0.41, 0.41, 0.41, 1.00)
        colors[clr.ScrollbarGrabActive]    = ImVec4(0.51, 0.51, 0.51, 1.00)
end

function style12()
    imgui.SwitchContext()

    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    local ImVec2 = imgui.ImVec2
	
    --==[ STYLE ]==--
    imgui.GetStyle().WindowPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().FramePadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().ItemSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().ItemInnerSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().TouchExtraPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().IndentSpacing = 5
    imgui.GetStyle().ScrollbarSize = 10
    imgui.GetStyle().GrabMinSize = 17

    --==[ ROUNDING ]==--
    imgui.GetStyle().WindowRounding = 5
    imgui.GetStyle().FrameRounding = 4
    imgui.GetStyle().ScrollbarRounding = 4
    imgui.GetStyle().GrabRounding = 2
	
    --==[ ALIGN ]==--
    imgui.GetStyle().WindowTitleAlign = imgui.ImVec2(0.5, 0.5)
    imgui.GetStyle().ButtonTextAlign = imgui.ImVec2(0.5, 0.5)
    
        colors[clr.WindowBg]               = ImVec4(0.0, 0.0, 0.0, 1.00)
        colors[clr.Text]                   = ImVec4(1.00, 1.00, 1.00, 1.00)
        colors[clr.TextDisabled]           = ImVec4(0.50, 0.50, 0.50, 1.00)
        colors[clr.PopupBg]                = ImVec4(0.08, 0.08, 0.08, 0.94)
        colors[clr.Border]                 = ImVec4(1.0, 1.0, 1.0, 0.10)
        colors[clr.BorderShadow]           = ImVec4(0.00, 0.00, 0.00, 0.00)
        colors[clr.FrameBg]                = ImVec4(0.44, 0.44, 0.44, 0.54)
        colors[clr.FrameBgHovered]         = ImVec4(0.57, 0.57, 0.57, 0.70)
        colors[clr.FrameBgActive]          = ImVec4(0.76, 0.76, 0.76, 0.80)
        colors[clr.TitleBg]                = ImVec4(0.04, 0.04, 0.04, 1.00)
        colors[clr.TitleBgActive]          = ImVec4(0.16, 0.16, 0.16, 1.00)
        colors[clr.TitleBgCollapsed]       = ImVec4(0.00, 0.00, 0.00, 0.60)
        colors[clr.MenuBarBg]              = ImVec4(0.14, 0.14, 0.14, 1.00)
        colors[clr.ScrollbarBg]            = ImVec4(0.02, 0.02, 0.02, 0.53)
        colors[clr.ScrollbarGrab]          = ImVec4(0.31, 0.31, 0.31, 1.00)
        colors[clr.ScrollbarGrabHovered]   = ImVec4(0.41, 0.41, 0.41, 1.00)
        colors[clr.ScrollbarGrabActive]    = ImVec4(0.51, 0.51, 0.51, 1.00)
        colors[clr.CheckMark]              = ImVec4(0.13, 0.75, 0.55, 0.80)
        colors[clr.SliderGrab]             = ImVec4(0.13, 0.75, 0.75, 0.80)
        colors[clr.SliderGrabActive]       = ImVec4(0.13, 0.75, 1.00, 0.80)
        colors[clr.Button]                 = ImVec4(0.13, 0.75, 0.55, 0.40)
        colors[clr.ButtonHovered]          = ImVec4(0.13, 0.75, 0.75, 0.60)
        colors[clr.ButtonActive]           = ImVec4(0.13, 0.75, 1.00, 0.80)
        colors[clr.Header]                 = ImVec4(0.13, 0.75, 0.55, 0.40)
        colors[clr.HeaderHovered]          = ImVec4(0.13, 0.75, 0.75, 0.60)
        colors[clr.HeaderActive]           = ImVec4(0.13, 0.75, 1.00, 0.80)
        colors[clr.Separator]              = ImVec4(0.13, 0.75, 0.55, 0.40)
        colors[clr.SeparatorHovered]       = ImVec4(0.13, 0.75, 0.75, 0.60)
        colors[clr.SeparatorActive]        = ImVec4(0.13, 0.75, 1.00, 0.80)
        colors[clr.ResizeGrip]             = ImVec4(0.13, 0.75, 0.55, 0.40)
        colors[clr.ResizeGripHovered]      = ImVec4(0.13, 0.75, 0.75, 0.60)
        colors[clr.ResizeGripActive]       = ImVec4(0.13, 0.75, 1.00, 0.80)
        colors[clr.PlotLines]              = ImVec4(0.61, 0.61, 0.61, 1.00)
        colors[clr.PlotLinesHovered]       = ImVec4(1.00, 0.43, 0.35, 1.00)
        colors[clr.PlotHistogram]          = ImVec4(0.90, 0.70, 0.00, 1.00)
        colors[clr.PlotHistogramHovered]   = ImVec4(1.00, 0.60, 0.00, 1.00)
        colors[clr.TextSelectedBg]         = ImVec4(0.26, 0.59, 0.98, 0.35)
end

function style13()
    imgui.SwitchContext()

    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    local ImVec2 = imgui.ImVec2
	
    --==[ STYLE ]==--
    imgui.GetStyle().WindowPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().FramePadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().ItemSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().ItemInnerSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().TouchExtraPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().IndentSpacing = 5
    imgui.GetStyle().ScrollbarSize = 10
    imgui.GetStyle().GrabMinSize = 17

    --==[ ROUNDING ]==--
    imgui.GetStyle().WindowRounding = 5
    imgui.GetStyle().FrameRounding = 4
    imgui.GetStyle().ScrollbarRounding = 4
    imgui.GetStyle().GrabRounding = 2
	
    --==[ ALIGN ]==--
    imgui.GetStyle().WindowTitleAlign = imgui.ImVec2(0.5, 0.5)
    imgui.GetStyle().ButtonTextAlign = imgui.ImVec2(0.5, 0.5)
    
        colors[clr.WindowBg]               = ImVec4(0.0, 0.0, 0.0, 1.00)
        colors[clr.PopupBg]                = ImVec4(0.08, 0.08, 0.08, 0.96)
        colors[clr.Border]                 = ImVec4(1.0, 1.0, 1.0, 0.10)
        colors[clr.FrameBg]                = ImVec4(0.49, 0.24, 0.00, 0.54)
        colors[clr.FrameBgHovered]         = ImVec4(0.65, 0.32, 0.00, 1.00)
        colors[clr.FrameBgActive]          = ImVec4(0.73, 0.36, 0.00, 1.00)
        colors[clr.TitleBg]                = ImVec4(0.15, 0.11, 0.09, 1.00)
        colors[clr.TitleBgActive]          = ImVec4(0.73, 0.36, 0.00, 1.00)
        colors[clr.TitleBgCollapsed]       = ImVec4(0.15, 0.11, 0.09, 0.51)
        colors[clr.MenuBarBg]              = ImVec4(0.62, 0.31, 0.00, 1.00)
        colors[clr.CheckMark]              = ImVec4(1.00, 0.49, 0.00, 1.00)
        colors[clr.SliderGrab]             = ImVec4(0.84, 0.41, 0.00, 1.00)
        colors[clr.SliderGrabActive]       = ImVec4(0.98, 0.49, 0.00, 1.00)
        colors[clr.Button]                 = ImVec4(0.73, 0.36, 0.00, 0.40)
        colors[clr.ButtonHovered]          = ImVec4(0.73, 0.36, 0.00, 1.00)
        colors[clr.ButtonActive]           = ImVec4(1.00, 0.50, 0.00, 1.00)
        colors[clr.Header]                 = ImVec4(0.49, 0.24, 0.00, 1.00)
        colors[clr.HeaderHovered]          = ImVec4(0.70, 0.35, 0.01, 1.00)
        colors[clr.HeaderActive]           = ImVec4(1.00, 0.49, 0.00, 1.00)
        colors[clr.SeparatorHovered]       = ImVec4(0.49, 0.24, 0.00, 0.78)
        colors[clr.SeparatorActive]        = ImVec4(0.49, 0.24, 0.00, 1.00)
        colors[clr.ResizeGrip]             = ImVec4(0.48, 0.23, 0.00, 1.00)
        colors[clr.ResizeGripHovered]      = ImVec4(0.78, 0.38, 0.00, 1.00)
        colors[clr.ResizeGripActive]       = ImVec4(1.00, 0.49, 0.00, 1.00)
        colors[clr.PlotLines]              = ImVec4(0.83, 0.41, 0.00, 1.00)
        colors[clr.PlotLinesHovered]       = ImVec4(1.00, 0.99, 0.00, 1.00)
        colors[clr.PlotHistogram]          = ImVec4(0.93, 0.46, 0.00, 1.00)
        colors[clr.TextSelectedBg]         = ImVec4(0.26, 0.59, 0.98, 0.00)
        colors[clr.ScrollbarBg]            = ImVec4(0.00, 0.00, 0.00, 0.53)
        colors[clr.ScrollbarGrab]          = ImVec4(0.33, 0.33, 0.33, 1.00)
        colors[clr.ScrollbarGrabHovered]   = ImVec4(0.39, 0.39, 0.39, 1.00)
        colors[clr.ScrollbarGrabActive]    = ImVec4(0.48, 0.48, 0.48, 1.00)
end

function style14()
    imgui.SwitchContext()

    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    local ImVec2 = imgui.ImVec2
	
    --==[ STYLE ]==--
    imgui.GetStyle().WindowPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().FramePadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().ItemSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().ItemInnerSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().TouchExtraPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().IndentSpacing = 5
    imgui.GetStyle().ScrollbarSize = 10
    imgui.GetStyle().GrabMinSize = 17

    --==[ ROUNDING ]==--
    imgui.GetStyle().WindowRounding = 5
    imgui.GetStyle().FrameRounding = 4
    imgui.GetStyle().ScrollbarRounding = 4
    imgui.GetStyle().GrabRounding = 2
	
    --==[ ALIGN ]==--
    imgui.GetStyle().WindowTitleAlign = imgui.ImVec2(0.5, 0.5)
    imgui.GetStyle().ButtonTextAlign = imgui.ImVec2(0.5, 0.5)
    
        colors[clr.FrameBg]                = ImVec4(0.16, 0.29, 0.48, 0.54)
        colors[clr.FrameBgHovered]         = ImVec4(0.26, 0.59, 0.98, 0.40)
        colors[clr.FrameBgActive]          = ImVec4(0.26, 0.59, 0.98, 0.67)
        colors[clr.TitleBg]                = ImVec4(0.04, 0.04, 0.04, 1.00)
        colors[clr.TitleBgActive]          = ImVec4(0.16, 0.29, 0.48, 1.00)
        colors[clr.TitleBgCollapsed]       = ImVec4(0.00, 0.00, 0.00, 0.51)
        colors[clr.CheckMark]              = ImVec4(0.26, 0.59, 0.98, 1.00)
        colors[clr.SliderGrab]             = ImVec4(0.24, 0.52, 0.88, 1.00)
        colors[clr.SliderGrabActive]       = ImVec4(0.26, 0.59, 0.98, 1.00)
        colors[clr.Button]                 = ImVec4(0.26, 0.59, 0.98, 0.40)
        colors[clr.ButtonHovered]          = ImVec4(0.26, 0.59, 0.98, 1.00)
        colors[clr.ButtonActive]           = ImVec4(0.06, 0.53, 0.98, 1.00)
        colors[clr.Header]                 = ImVec4(0.26, 0.59, 0.98, 0.31)
        colors[clr.HeaderHovered]          = ImVec4(0.26, 0.59, 0.98, 0.80)
        colors[clr.HeaderActive]           = ImVec4(0.26, 0.59, 0.98, 1.00)
        colors[clr.Separator]              = colors[clr.Border]
        colors[clr.SeparatorHovered]       = ImVec4(0.26, 0.59, 0.98, 0.78)
        colors[clr.SeparatorActive]        = ImVec4(0.26, 0.59, 0.98, 1.00)
        colors[clr.ResizeGrip]             = ImVec4(0.26, 0.59, 0.98, 0.25)
        colors[clr.ResizeGripHovered]      = ImVec4(0.26, 0.59, 0.98, 0.67)
        colors[clr.ResizeGripActive]       = ImVec4(0.26, 0.59, 0.98, 0.95)
        colors[clr.TextSelectedBg]         = ImVec4(0.26, 0.59, 0.98, 0.35)
        colors[clr.Text]                   = ImVec4(1.00, 1.00, 1.00, 1.00)
        colors[clr.TextDisabled]           = ImVec4(0.50, 0.50, 0.50, 1.00)
        colors[clr.WindowBg]               = ImVec4(0.0, 0.0, 0.0, 1.00)
        colors[clr.PopupBg]                = ImVec4(0.08, 0.08, 0.08, 0.94)
        colors[clr.Border]                 = ImVec4(0.43, 0.43, 0.50, 0.20)
        colors[clr.BorderShadow]           = ImVec4(0.00, 0.00, 0.00, 0.00)
        colors[clr.MenuBarBg]              = ImVec4(0.14, 0.14, 0.14, 1.00)
        colors[clr.ScrollbarBg]            = ImVec4(0.02, 0.02, 0.02, 0.53)
        colors[clr.ScrollbarGrab]          = ImVec4(0.31, 0.31, 0.31, 1.00)
        colors[clr.ScrollbarGrabHovered]   = ImVec4(0.41, 0.41, 0.41, 1.00)
        colors[clr.ScrollbarGrabActive]    = ImVec4(0.51, 0.51, 0.51, 1.00)
        colors[clr.PlotLines]              = ImVec4(0.61, 0.61, 0.61, 1.00)
        colors[clr.PlotLinesHovered]       = ImVec4(1.00, 0.43, 0.35, 1.00)
        colors[clr.PlotHistogram]          = ImVec4(0.90, 0.70, 0.00, 1.00)
        colors[clr.PlotHistogramHovered]   = ImVec4(1.00, 0.60, 0.00, 1.00)
end
